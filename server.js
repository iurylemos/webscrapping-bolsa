var http = require("http");
var url = require("url");
var path = require("path");
var fs = require("fs");

var scraper = require('./scraper');
var port = process.argv[2] || 8888;

const server = http.createServer(function (request, response) {

  var uri = url.parse(request.url).pathname
    , filename = path.join(process.cwd(), uri);


  fs.exists(filename, function (exists) {
    if (!exists) {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += '/index.html';

    fs.readFile(filename, "binary", function (err, file) {
      if (err) {
        response.writeHead(500, { "Content-Type": "text/plain" });
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
})




console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");

var io = require('socket.io')(server);

io.on('connection', async (socket) => {

  console.log('user connected');

  await scraper.open()

  setInterval(async () => {

    // get the time every second
    const time = await scraper.getTime();

    console.log('time', time)
    if (time) {
      fs.writeFile('valoresBolsa.json', JSON.stringify(values, null, 2), err => {
        if (err) throw new Error('somenthing went wrong');
        console.log('well done!')
      })

      // emit the updated time
      socket.emit("refresh", time);
    }


  }, 10000);

  let filename = path.join(process.cwd(), uri);


  fs.exists(filename, function (exists) {
    if (!exists) {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += '/index.html';

    fs.readFile(filename, "binary", function (err, file) {
      if (err) {
        response.writeHead(500, { "Content-Type": "text/plain" });
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
})

server.listen(parseInt(port, 10));