const puppeteer = require('puppeteer');
const fs = require('fs');
/**
 * Scraper
 * Use this instead of scrape variable
 */
let browser, page;
const scraper = {

  async open() {
    browser = await puppeteer.launch({ headless: true });
    page = await browser.newPage();
    const url = "https://statusinvest.com.br/acoes/itsa4";
    await page.goto(url);
    await page.waitFor(1000);
  },

  async getTime() {

    const selector = 'div .top-info.has-special'

    return await page.evaluate((selector) => {
      const nodeList = document.querySelectorAll(selector);
      const list = [...nodeList].map((resp) => resp.children)
      const values = [...list[0]].map((arr) => arr.innerText.split("\n"))
      // console.log('values', values)
      return values
    }, selector);
  }
};

module.exports = scraper;